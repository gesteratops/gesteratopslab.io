import { defineConfig } from 'astro/config';

// https://astro.build/config
export default defineConfig({
  site: 'https://gesteratops.gitlab.io',
  outDir: 'public',
  publicDir: 'static',
});
